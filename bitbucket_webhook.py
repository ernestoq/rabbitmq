#!/usr/bin/env python3

import socket
import time


HOST = "localhost"  
PORT = 49199      

try:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        s.send(bytes("fake commit", "utf-8"))
        # recv message
        message = s.recv(1024)
        message = message.decode()  
        print(message)

except ConnectionRefusedError as error_msg:
    print(f"ERROR: {error_msg}")
        
