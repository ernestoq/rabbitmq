#!/usr/bin/env python3

import socket
import threading
import sys
import logging
import json
import re
import subprocess
import requests
from requests.auth import HTTPBasicAuth


def log(log):

    # Logger configuration
    logging.basicConfig(level=logging.INFO, filename="/home/ec2-user/listener.log", 
                        filemode="w", format="%(name)s - %(levelname)s - %(message)s")
    logger = logging.getLogger()
    logger.info(log)


def publish_message(msg):

    # Build API call
    rabbitmq_url = "http://ec2-3-84-248-30.compute-1.amazonaws.com:49156/api/exchanges/%2F/bitbucket_exchange/publish"
    body = {
            "vhost": "/", 
            "name": "bitbucket_exchange", 
            "routing_key": "", 
            "payload": f"{msg}", 
            "payload_encoding": "string", 
            "properties": {
                "headers": {}, 
                "delivery_mode": 2}
            }
    result = requests.post(rabbitmq_url, json=body, auth=HTTPBasicAuth('fido_admin', 'fido_admin'))
    if result.status_code == 200:
        log("message published successfully")
    else:
        log("message was not published")


def message(conn, addr):

    # Recv message
    msg = conn.recv(2048)
    msg = msg.decode("utf-8")

    # Send ACK and close socket
    ack_msg = "ack"
    ack_msg = ack_msg.encode()
    conn.send(ack_msg)
    conn.close()

    # Parse message

    """
       If User-Agent is Bitbucket, new entry is created in listener.log 
       and a git pull is executed under fido/ folder.

       If User-Agent does not match, new entry is created in listener.log.
    """

    result = re.search("User-Agent:.+", msg)
    if result == None:
        log(f"client: {addr}, message ignored: User-Agent mismatch")

    elif result != None:
        if "Bitbucket" in result.group():
            log(f"client: {addr}, recv: {result.group()}")
            publish_message("new commit")
        else:
            log(f"client: {addr}, message ignored: User-Agent mismatch")
    
   
def build_socket():

    # Build socket
    # socket allows connections from any hosts with 'host = ""'
    host = ""
    port = 49199
    s = socket.socket()
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) 
    s.bind((host, port))
    s.listen(1)
    return s


def main():

    print("Listening on port 49199/tcp")
    try:
        s = build_socket()
        threads_list = []
        while True:
            conn, addr = s.accept()
            t = threading.Thread(target=message, args=(conn, addr))
            t.start()
            threads_list.append(t)
  
    except KeyboardInterrupt:
        print("Listener was terminated.")
    except Exception as error:
        print(f"Error: {error}")

    finally:
        # Close socket and wait for threads to finish
        for t in threads_list:
            t.join()
        s.close()


main()
