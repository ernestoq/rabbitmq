#!/usr/bin/env python3


import pika
import sys
import logging
import subprocess


def log(log):

    # Logger configuration
    logging.basicConfig(level=logging.INFO, filename="/home/ec2-user/listener.log", 
                        filemode="w", format="%(name)s - %(levelname)s - %(message)s")
    logger = logging.getLogger()
    logger.info(log)


def git_pull(channel, method, properties, body):

    # Run git pull under myrepo folder
    try:
        cmd_path = "/home/ec2-user/fido"
        result = subprocess.run(["git", "pull"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, cwd=cmd_path)
        if 0 == result.returncode:
            log("git pull successful")
            print(f"Data received from bitbucket queue")
    except Exception as error:
        log(error)


def build_channel():
    
    try:
        rabbitmq_url = "http://fido_admin:fido_admin@ec2-3-84-248-30.compute-1.amazonaws.com:49158/%2F/"
        params = pika.URLParameters(rabbitmq_url)
        connection = pika.BlockingConnection(params)
        channel = connection.channel() 
        return channel

    except:
        print("ERROR: Channel not created")
        sys.exit()
    

def main():
 
    # Subscribe consumer to bitbucket queue
    channel = build_channel()
    channel.basic_consume("bitbucket_queue", git_pull, auto_ack=True)
    # Start channel
    channel.start_consuming()
    

main()
